﻿namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Interfaces
{
    public interface IPromocodeDatabaseSettings
    {
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}