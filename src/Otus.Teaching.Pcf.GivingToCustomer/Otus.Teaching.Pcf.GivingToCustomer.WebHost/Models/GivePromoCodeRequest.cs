﻿using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models
{
    /// <example>
    /// {
    ///   "serviceInfo": "info",
    ///   "partnerId": "978dc8da-b1dd-4951-a637-dd29cd2a967f",
    ///   "promoCodeId": "f3095218-f075-4407-89ad-0a14b9c2c739",
    ///   "promoCode": "promo",
    ///   "preferenceId": "76324c47-68d2-472d-abb8-33cfa8cc0c84",
    ///   "beginDate": "2020-01-01",
    ///    "endDate": "2022-01-01"
    /// }
    /// </example>>
    public class GivePromoCodeRequest
    {
        public string ServiceInfo { get; set; }

        public Guid PartnerId { get; set; }

        public Guid PromoCodeId { get; set; }
        
        public string PromoCode { get; set; }

        public Guid PreferenceId { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }
    }
}