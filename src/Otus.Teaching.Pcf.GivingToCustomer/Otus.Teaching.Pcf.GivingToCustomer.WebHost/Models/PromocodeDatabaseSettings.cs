﻿using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Interfaces;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models
{
    public class PromocodeDatabaseSettings : IPromocodeDatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}