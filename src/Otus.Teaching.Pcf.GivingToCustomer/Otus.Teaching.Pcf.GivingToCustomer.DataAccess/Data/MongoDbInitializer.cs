﻿using System;
using System.Threading;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Interfaces;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer :
            IDbInitializer
    {
        private IMongoDatabase _database;

        public MongoDbInitializer(IPromocodeDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            _database = client.GetDatabase(settings.DatabaseName);
        }

        public void InitializeDb()
        {
            _database.DropCollection(nameof(Preference));
            _database.DropCollection(nameof(Customer));
            _database.DropCollection(nameof(PromoCode));
            _database.DropCollection(nameof(CustomerPreference));
            _database.DropCollection(nameof(PromoCodeCustomer));

            var customers = _database.GetCollection<Customer>(nameof(Customer));
            var prefers = _database.GetCollection<Preference>(nameof(Preference));
            var customerPereference = _database.GetCollection<CustomerPreference>(nameof(CustomerPreference));
            var promoCodesCustomer = _database.GetCollection<PromoCodeCustomer>(nameof(PromoCodeCustomer));

            prefers.InsertMany(FakeDataFactory.Preferences);
            customers.InsertMany(FakeDataFactory.Customers);
            
            FakeDataFactory.Customers.ForEach(c =>
            {
                if(c.Preferences!=null)
                    customerPereference.InsertMany(c.Preferences);
                if (c.PromoCodes != null)
                    promoCodesCustomer.InsertMany(c.PromoCodes);
            });
            
        }
    }
}