﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Interfaces;
using MongoDB.Driver;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class MongoRepository<T> : IRepository<T> where T :
            BaseEntity
    {
        private readonly IMongoCollection<T> _collection;
        public MongoRepository(IPromocodeDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _collection = database.GetCollection<T>(typeof(T).Name);
        }

        public async Task AddAsync(T entity)
        {
            await _collection.InsertOneAsync(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await _collection.FindOneAndDeleteAsync(e => e.Id == entity.Id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var result = await (await _collection.FindAsync(t => true)).ToListAsync();

            return result;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var filter = Builders<T>.Filter.Eq(doc => doc.Id, id);
            var result = await _collection.Find(filter).SingleOrDefaultAsync();
            
            return result;
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            var result = await (await _collection.FindAsync(predicate)).FirstOrDefaultAsync();
            
            return result;
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var result = await (await _collection.FindAsync(c => ids.Contains(c.Id))).ToListAsync();

            return result;
        }

        public Task<IEnumerable<T>> GetWithIncludeAsync(params Expression<Func<T, object>>[] includeProperties)
        {
            return Task.FromResult(Include(includeProperties).AsEnumerable());
        }

        public Task<IEnumerable<T>> GetWithIncludeAsync(Func<T, bool> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            var query = Include(includeProperties);
            return Task.FromResult(query.Where(predicate).AsEnumerable());
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            var result = await  (await _collection.FindAsync(predicate)).ToListAsync();

            return result;
        }

        public async Task UpdateAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq(doc => doc.Id, entity.Id);

            await _collection.FindOneAndReplaceAsync(filter, entity);
        }

        private IQueryable<T> Include(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _collection.AsQueryable();
            return includeProperties
                    .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }
    }
}